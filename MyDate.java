import java.util.Calendar;
import java.util.Scanner;

/**
 * Created by NguyenPhu on 30/9/2016.
 */

public class MyDate {
    private int date, month, year;
    public MyDate() {
        Calendar day = Calendar.getInstance();
        date = day.get(Calendar.DAY_OF_MONTH);
        month = day.get(Calendar.MONTH);
        year = day.get(Calendar.YEAR);
    }

    public MyDate(int dd, int mm, int yy) {
        date = dd;
        month = mm;
        year = yy;
    }

    public void nhapdl() {
        Scanner input = new Scanner(System.in);
        System.out.print("Nhap vao ngay: ");
        date = input.nextInt();
        System.out.print("Nhap vao thang: ");
        month = input.nextInt();
        System.out.print("Nhap vao nam: ");
        year = input.nextInt();
    }

    public void ShowKQ() {
        System.out.println(date + "/" + month + "/" + year);
    }

    public static void main(String[] args) {
        MyDate date1 = new MyDate();
        date1.ShowKQ();

        MyDate date2 = new MyDate(1, 1, 1996);
        date2.ShowKQ();

        MyDate date3 = new MyDate();
        date3.nhapdl();
        date3.ShowKQ();
    }
}
